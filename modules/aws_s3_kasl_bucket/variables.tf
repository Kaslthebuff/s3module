variable "aws_region" {
  description = "Region in which AWS resources to be created"
  type        = string
}
variable "aws_profile" {
  description = "AWS credential profile"
  type        = string
}

variable "bucket_name" {
  description = "the name of the bucket"
  type        = string
}

variable "bucket-tags" {
  description = "S3 bucket tags"
  type        = map(string)
  default = {
  }
}