output "arn" {
  description = "ARN of the bucket"
  value       = aws_s3_bucket.kasl_bucket.arn
}

output "name" {
  description = "name of the bucket"
  value       = aws_s3_bucket.kasl_bucket.id
}

output "domain" {
  description = "Domain name of the bucket"
  value       = aws_s3_bucket.kasl_bucket.website_domain
}

output "endpoint" {
  description = "endpoint of the bucket"
  value       = aws_s3_bucket.kasl_bucket.website_endpoint
}
